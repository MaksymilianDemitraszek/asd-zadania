#include <iostream>
#include <vector>
#include "Sort.h"

bool isSorted(std::vector<int>* v) {
    if (v->empty()) return true;
    int el = *v->begin();
    for(std::vector<int>::iterator it = ++v->begin(); it != v->end(); ++it){
        if(el > *it) return false;
        el = *it;
    }
    return true;
}

void fillVector(std::vector<int>* v) {
    for(int i = 1011; i > -1551; i -= 5) {
        v->push_back(i);
    }
}

int main() {
    Sort* s = new Sort();
    std::vector<int>* first = new std::vector<int>();
    fillVector(first);
    std::vector<int>* second = new std::vector<int>();
    fillVector(second);
    std::vector<int>* third = new std::vector<int>();
    fillVector(third);

    s->setArray(first);
    s->sort();
    std::cout << "Is sorted by insertion sort correctly?: \n";
    std::cout << (isSorted(first) ? "true" : "false");
    std::cout << std::endl;

    s->setArray(second);
    s->mergeSort();
    std::cout << "Is sorted by merge sort correctly?: \n";
    std::cout << (isSorted(second) ? "true" : "false");
    std::cout << std::endl;

    s->setArray(third);
    s->heapSort();
    std::cout << "Is sorted by heap sort correctly?: \n";
    std::cout << (isSorted(third) ? "true" : "false");
    std::cout << std::endl;
}
