#include <vector>

class Sort {
private:
    std::vector<int>* array;
    void mergeS(int l, int r);
    void merge(int l, int p, int r);
    void heapify(int index, int size);
public:
    Sort()=default;
    void setArray(std::vector<int>* arrayNew);
    void sort();
    void mergeSort();
    void heapSort();
};