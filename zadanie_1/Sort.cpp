#include <vector>
#include "Sort.h"

void Sort::setArray(std::vector<int> *arrayNew) {
    this->array = arrayNew;
}

void Sort::sort() {
    for (int j = 1; j < this->array->size(); j++) {
        int el = (*this->array)[j];
        int i = j - 1;
        while (i >= 0 && (*this->array)[i] > el) {
            (*this->array)[i + 1] = (*this->array)[i];
            i--;
        }
        (*this->array)[i + 1] = el;
    }
}

void Sort::merge(int l, int p, int r){
    int leftSize  = p - l + 1;
    int *left = new int[leftSize];
    int rightSize = r - p;
    int *right = new int[rightSize];
    for(int i = 0, w = l; i < leftSize; i ++, w++){
        left[i] = this->array->at(w);
    }
    for(int i = 0, w = p + 1; i < rightSize; i ++, w++){
        right[i] = this->array->at(w);
    }
    int lIter = 0, rIter = 0, org = l;
    while( org < r + 1 ){
        if(left[lIter] <= right[rIter] ){
            this->array->at(org) = left[lIter];
            lIter++;
        }else{
            this->array->at(org) = right[rIter];
            rIter++;
        }
        org++;

        if(lIter == leftSize){
            break;
        }
        if(rIter == rightSize){
            break;
        }
    }
    while(lIter == leftSize && org < r+ 1){
        this->array->at(org) = right[rIter];
        rIter ++;
        org++;
    }
    while(rIter == rightSize && org < r+ 1){
        this->array->at(org) = left[lIter];
        lIter++;
        org++;
    }

    delete[] left;
    delete[] right;
}

void Sort::mergeS(int l, int r){
    if(l == r ) return;
    int p = (l+r)/2;
    mergeS(l, p);
    mergeS(p+1, r);
    merge(l, p, r);

}

void Sort::mergeSort(){
    mergeS(0, this->array->size() - 1);
}

void Sort::heapSort() {
    for (int i = this->array->size() / 2 - 1; i >= 0; i -- )
        this->heapify(i, this->array->size());
    int size = this->array->size();
    for (int i = size - 1; i > 0; i --){
        int cont = this->array->at(0);
        this->array->at(0) = this->array->at(size - 1);
        this->array->at(size - 1) = cont;
        size--;
        heapify(0, size);
    }
}

void Sort::heapify(int index, int size) {
    int topIndex = index, left = 2*index + 1, right = left + 1;
    if (left < size && this->array->at(left) > this->array->at(topIndex))
        topIndex = left;
    if (right < size && this->array->at(right) > this->array->at(topIndex))
        topIndex = right;
    if(topIndex != index) {
        int cont = this->array->at(index);
        this->array->at(index) = this->array->at(topIndex);
        this->array->at(topIndex) = cont;
        heapify(topIndex, size);
    }
}
